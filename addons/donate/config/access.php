﻿<?php
return array(
	'modules' => array(
		'donate' => array(
            'main'					=> AccountLevel::ANYONE,
			'pgs' 	   				=> AccountLevel::NORMAL,
            'pgs-history'			=> AccountLevel::NORMAL,
			'pgs-return'			=> AccountLevel::ANYONE,
			'pgs-process'			=> AccountLevel::NORMAL,
		),
		
		'modules' => array(
			'cplog' => array(
				'log' 				=> AccountLevel::ADMIN,
				'view' 				=> AccountLevel::ADMIN,
				'statistics' 		=> AccountLevel::ADMIN,
				'about' 			=> AccountLevel::ADMIN,
			),
		),
	),
)
?>