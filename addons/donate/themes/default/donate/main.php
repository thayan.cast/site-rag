	<div class="row">
		<div class="col-12">
			<h2 class="head">Doações</h2>
			<p class="sub-head">Faça uma Doação e ajude nosso servidor a crescer!</p>
			<p class="sub-head"><strong>Escolha uma das opções para continuar com a doação.</strong></p>
		</div>
	</div>
	<div class="row donate-page">
		<div class="col-12 col-sm-12 col-md-6">
			<a href="<?php echo $this->url('donate','pgs'); ?>" class="item green">
				<img src="<?php echo $this->themePath('images/render/donate-pgs.png') ?>">
				
			</a>
		</div>
		<div class="col-12 col-sm-12 col-md-6">
			<a href="<?php echo $this->url('donate'); ?>" class="item blue">
				<img src="<?php echo $this->themePath('images/render/donate-paypal.png') ?>">
				
			</a>
		</div>
	</div>