﻿<?php
return array(
	'modules' => array(
		'ranking' => array(
            'pvp'					=> AccountLevel::ANYONE,
            'gvg'					=> AccountLevel::ANYONE,
            'ggvg'					=> AccountLevel::ANYONE,
            'break'					=> AccountLevel::ANYONE,
            'mvp2'					=> AccountLevel::ANYONE,
			'woe' 	   				=> AccountLevel::ANYONE,
		),
	),
)
?>