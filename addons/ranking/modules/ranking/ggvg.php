<?php
if (!defined('FLUX_ROOT')) exit;

$CCfg = require($this->themePath('config.php', true));

$title    = 'Top Clãs GvG';
$classes  = Flux::config('JobClasses')->toArray();
$jobClass = $params->get('jobclass');
$bind     = array();
$sqlpartial = '';

if (trim($jobClass) === '') {
	$jobClass = null;
}

if (!is_null($jobClass) && !array_key_exists($jobClass, $classes)) {
	$this->deny();
}

$col = "`rank`.`guild_id` AS `guild_id`, `rank`.`kill` AS `kill`, `rank`.`die` AS `die`, (`rank`.`kill`-`rank`.`die`) AS ration, `rank`.`last_update` AS `last_update`, ";
$col .= "`guild`.`char_id`, `guild`.`name` AS `guild_name`, `guild`.`emblem_len` AS `guild_emblem_len`, ";
$col .= "`ch`.`name` AS `name`, `ch`.`class` AS `job`, `ch`.`base_level` AS `base_level`, `ch`.`job_level` AS `job_level`, `ch`.`sex` AS `sex` ";

$sqlpartial .= "FROM {$server->charMapDatabase}.`ggvg_flux_csd` AS `rank`";
$sqlpartial .= "LEFT JOIN {$server->charMapDatabase}.`guild` AS `guild` ON `guild`.`guild_id` = `rank`.`guild_id` ";
$sqlpartial .= "LEFT JOIN `{$server->charMapDatabase}`.`char` AS `ch` ON `ch`.`char_id`=`guild`.`char_id` ";
$sqlpartial .= "WHERE 1=1 ";

// Get total count and feed back to the paginator.
$sth = $server->connection->getStatement("SELECT COUNT(DISTINCT `rank`.`guild_id`) AS total $sqlpartial");
$sth->execute($bind);
$total = $sth->fetch()->total;
$paginator = $this->getPaginator($total);
$sortable = array('ration' => 'desc', 'last_update' => 'asc');
$paginator->setSortableColumns($sortable);

$sql  = $paginator->getSQL("SELECT $col $sqlpartial GROUP BY `rank`.`guild_id`");
$sth  = $server->connection->getStatement($sql);

$sth->execute($bind);
$guilds = $sth->fetchAll();
?>
