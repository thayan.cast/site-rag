<?php if (!defined('FLUX_ROOT')) exit; 
	$limit=(int)Flux::config('CharRankingLimit');
	if( $limit > $total )
		$limit = $total;
?>
<h2 class="head">Top PvP</h2>
<p class="sub-head">
	Listando <?php echo number_format($limit) ?> de <?php echo number_format($total) ?>  Jogadores
	<?php if (!is_null($jobClass)): ?>
	(<?php echo htmlspecialchars($className=$this->jobClassText($jobClass)) ?>)
	<?php endif ?>
</p>
<?php if ($chars): ?>
<form action="" method="get" class="search-form2 float-right">
	<?php echo $this->moduleActionFormInputs('ranking', 'pvp') ?>
	<p class="no-border">
		<label for="jobclass">Filtrar pela Classe:</label>
		<br/>
		<select name="jobclass" id="jobclass">
			<option value=""<?php if (is_null($jobClass)) echo 'selected="selected"' ?>>All</option>
		<?php foreach ($classes as $jobClassIndex => $jobClassName): ?>
			<option value="<?php echo $jobClassIndex ?>"
				<?php if (!is_null($jobClass) && $jobClass == $jobClassIndex) echo ' selected="selected"' ?>>
				<?php echo htmlspecialchars($jobClassName) ?>
			</option>
		<?php endforeach ?>
		</select>
		
		<input type="submit" value="Filtrar" />
		<input type="button" value="Resetar" onclick="reload()" />
	</p>
</form>
<table class="table table-green">
	<thead>
		<tr>
			<th class="text-center" data-toggle="tooltip-xs" title="Posição">Pos<span class="d-none d-sm-none d-md-inline">ição</span></th>
			<th>Personagem</th>
			<th class="text-center" data-toggle="tooltip-xs" title="Classe">Cl<span class="d-none d-sm-none d-md-inline">asse</span></th>
			<th class="text-center" data-toggle="tooltip-xs" title="Clã">C<span class="d-none d-sm-none d-md-inline">lã</span></th>
			<th class="text-center d-none d-sm-none d-md-table-cell">Nível de Base</th>
			<th class="text-center d-none d-sm-none d-md-table-cell">Nível de Classe</th>
			<th class="text-center" data-toggle="tooltip-xs" title="Eliminações">E<span class="d-none d-sm-none d-md-inline">liminações</span></th>
			<th class="text-center" data-toggle="tooltip-xs" title="Mortes">M<span class="d-none d-sm-none d-md-inline">ortes</span></th>
			<th class="text-center" data-toggle="tooltip-xs" title="Proporção"><?php echo htmlspecialchars_decode($paginator->sortableColumn('ration', 'P<span class="d-none d-sm-none d-md-inline">roporção</span>')) ?></th>
		</tr>
	</thead>
	<tbody>
		<?php $topRankType = !is_null($jobClass) ? $className : 'character' ?>
		<?php
			$i=0;
			foreach( $chars as $crow ):
				++$i;
				$guildname = "";
				$sex = $crow->sex == 'F' ? 'f' : 'm';
				$class = isset($Creative->Cfg['JobAliasesGender'][$sex][$crow->job]) ? $Creative->Cfg['JobAliasesGender'][$sex][$crow->job] : $crow->job;
				$icon = isset($Creative->Cfg['JobAliases'][$class]) ? $Creative->Cfg['JobAliases'][$class] : $class;
				$charname = '<span data-toggle="tooltip-xs" title="<strong>Nível de Base:</strong> ' . $crow->base_level . '<br/><strong>Nível de Classe:</strong> ' . $crow->job_level . '">' . $crow->name . '</span>';
		?>
		<tr>
			<td class="text-center"><?php echo number_format($i) ?></td>
			<td><strong data-toggle="tooltip-xs" data-html="true" title="<strong>Nível de Base:</strong> <?php echo $crow->base_level ?><br/><strong>Nível de Classe:</strong> <?php echo $crow->job_level ?>">
				<?php if ($auth->actionAllowed('character', 'view') && $auth->allowedToViewCharacter): ?>
					<?php echo htmlspecialchars_decode($this->linkToCharacter($crow->char_id, $crow->name)) ?>
				<?php else: ?>
					<?php echo htmlspecialchars($crow->name) ?>
				<?php endif ?>
			</strong></td>
			<td class="text-center"><img src="<?php echo $this->themePath('images/class/guild/' . $icon . '.png') ?>" data-toggle="tooltip-xs" title="<?php echo $this->jobClassText($crow->job) ?>"> <span class="d-none d-sm-none d-md-inline"><?php echo $this->jobClassText($crow->job) ?></span></td>
			<?php if ($crow->guild_name): ?>
			<td class="text-center">
			<?php if ($crow->guild_emblem_len):
				$guildname = '<img src="' . $this->emblem($crow->guild_id) . '" data-toggle="tooltip-xs" title="' . $crow->guild_name .'" /> ';
			?>
			<?php endif; ?>
			<?php
				  $guildname .= '<span class="d-none d-sm-none d-md-inline">' . $crow->guild_name . '</span>';
			?> 
			<?php if (!$crow->guild_emblem_len) ?>
				<?php if ($auth->actionAllowed('guild', 'view') && $auth->allowedToViewGuild): ?>
					<?php echo htmlspecialchars_decode($this->linkToGuild($crow->guild_id, $guildname)) ?>
				<?php else: ?>
					<?php echo $guildname ?>
				<?php endif ?>
			</td>
			<?php else: ?>
			<td class="text-center"><span class="not-applicable">N/a</span></td>
			<?php endif ?>
			<td class="text-center d-none d-sm-none d-md-table-cell"><?php echo number_format($crow->base_level) ?></td>
			<td class="text-center d-none d-sm-none d-md-table-cell"><?php echo number_format($crow->job_level) ?></td>
			<td class="text-center"><?php echo number_format($crow->kill) ?></td>
			<td class="text-center"><?php echo number_format($crow->die) ?></td>
			<td class="text-center"><?php echo number_format($crow->ration) ?></td>
		</tr>
	<?php endforeach ?>
	</tbody>
</table>
<?php echo $paginator->getHTML() ?>
<?php else: ?>
<p>Não há personagens. <a href="javascript:history.go(-1)">Voltar</a>.</p>
<?php endif ?>
