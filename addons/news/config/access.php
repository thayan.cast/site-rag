﻿<?php
return array(
	'modules' => array(
		'cnews' => array(
            'index'					=> AccountLevel::ANYONE,
            'view'					=> AccountLevel::ANYONE,
            'add'					=> AccountLevel::ADMIN,
            'delete'				=> AccountLevel::ADMIN,
            'edit'					=> AccountLevel::ADMIN,
			'load'					=> AccountLevel::ANYONE,
		),
	),
)
?>