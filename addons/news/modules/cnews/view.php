<?php
if (!defined('FLUX_ROOT')) exit;

$title = 'Visualizando Notícia';
$news = Flux::config('FluxTables.CMSNewsTable');
$id	= trim($params->get('id'));
$sql = "SELECT id, title, body, type, link, author, created, modified FROM {$server->loginDatabase}.$news WHERE id=?";

$sth = $server->connection->getStatement($sql);
$sth->execute((array)$id);

$news = $sth->fetchAll();
if( $news )
	$title = $news[0]->title;
?>
