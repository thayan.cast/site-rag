<?php
if (!defined('FLUX_ROOT')) exit;

$Cfg = require($this->themePath('config.php',true));

$result = array();
$news = Flux::config('FluxTables.CMSNewsTable'); 
$category = $params->get('type');
$bind = array();

$sqlpartial = "WHERE 1=1 ";

if (trim($category) !== '' && $category !== 'all') {
	$sqlpartial .= "AND type=? ";
	$bind[] = $category;
}

$sql = "FROM {$server->loginDatabase}.$news {$sqlpartial}";
$sth = $server->connection->getStatement("SELECT COUNT(DISTINCT id) AS total {$sql}");
$sth->execute($bind);
$result['total'] = (int)$sth->fetch()->total;
if( $result['total'] ) {
	$sth = $server->connection->getStatement("SELECT id, type, title, created, modified {$sql} ORDER BY id DESC LIMIT {$Cfg['News']['home_rows']}");
	$sth->execute($bind);
	$news = $sth->fetchAll();
	$result['rows'] = array();
	foreach($news as $nrow) {
		$result['rows'][] = array(
			'id' => $nrow->id,
			'type' => $nrow->type,
			'title' => $nrow->title,
			'date_created' => date("d.m.Y",strtotime($nrow->created)),
			'hour_created' => date("H:i:s",strtotime($nrow->created)),
			'date_modified' => date("d-m-Y",strtotime($nrow->modified)),
			'hour_modified' => date("H:i:s",strtotime($nrow->modified)),
			'class' => isset($Cfg['News']['type'][$nrow->type]['class']) ? $Cfg['News']['type'][$nrow->type]['class'] : 'all',
			'label' => isset($Cfg['News']['type'][$nrow->type]['label']) ? utf8_encode($Cfg['News']['type'][$nrow->type]['label']) : utf8_encode('Notícias'),
			'url' => $this->url('cnews','view', array('id' => $nrow->id)),
		);
	}
}
echo json_encode($result);
exit;
?>
