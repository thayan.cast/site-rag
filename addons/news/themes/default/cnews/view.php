<?php 
if (!defined('FLUX_ROOT')) exit;
?>

<?php if($news): ?>
	<?php foreach($news as $nrow):?>
		<?php
			$date = date("d-m-Y H:i:s",strtotime($nrow->created));
			list($day,$month,$year,$hour,$min,$sec) = sscanf($date, "%d-%d-%d %d:%d:%d");
			$label = isset($Creative->Cfg['News']['type'][$nrow->type]['label']) ? $Creative->Cfg['News']['type'][$nrow->type]['label'] : null;
		?>
		<div class="row news-view">
			<div class="col-12">
				<div class="head">
					<h2><?php echo $nrow->title ?></h2>
					<p><small>Por <strong><?php echo $nrow->author ?></strong> em <strong><?php echo sprintf("%02d de %s de %d", $day, $Creative->Cfg['Months']['full'][$month], $year) ?></strong></small>
					<?php if( $label ): ?>
					<p class="tag"><a href="<?php echo $this->url('cnews','index', array('category' => $nrow->type)); ?>"><i class="fas fa-tag"></i> <?php echo utf8_encode($label) ?></a>
					<?php endif; ?>
				</div>
				<div class="content">
					<?php echo $nrow->body ?>
				</div>
				<div class="footer">
					<?php if($nrow->created != $nrow->modified && Flux::config('CMSDisplayModifiedBy')):?>
						<small><?php echo htmlspecialchars(Flux::message('CMSModifiedLabel')) ?> : <?php echo date('m-d-y',strtotime($nrow->modified))?></small>
					<?php endif; ?>
					<a href="<?php echo $this->url('cnews','view', array('id' => $nrow->id)); ?>"><i class="fab fa-readme"></i> Ler Notícia</a>
					<?php if($nrow->link): ?>
					| <a href="<?php echo $nrow->link ?>"><i class="fas fa-link"></i> Link Externo</a>
					<?php endif; ?>
					<?php if( $auth->actionAllowed('cnews', 'edit') ): ?>
					| <a href="<?php echo $this->url('cnews','edit', array('id' => $nrow->id)); ?>"><i class="fas fa-edit"></i> Editar</a>
					<?php endif; ?>
					<?php if( $auth->actionAllowed('cnews', 'delete') ): ?>
					| <a href="<?php echo $this->url('cnews','delete', array('id' => $nrow->id)); ?>" onclick="return confirm('<?php echo htmlspecialchars(Flux::message('CMSConfirmDeleteLabel')) ?>');"><i class="fas fa-trash-alt"></i> Excluír</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	<?php endforeach; ?> 
<?php else: ?>
	<div class="row">
		<div class="col-12">
			<h2 class="head"><?php echo htmlspecialchars($title) ?></h2>
			<p class="sub-head"><?php echo 'Visualizando artigos direcionados.' ?></p>
			<p class="mt-4"><?php echo htmlspecialchars(Flux::message('CMSNewsEmpty')) ?></p>
		</div>
	</div>
<?php endif ?>
