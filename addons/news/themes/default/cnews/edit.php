<?php 
if (!defined('FLUX_ROOT')) exit;
$this->loginRequired(); 
?>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>tinymce.init(
	{
		selector:'textarea',
		height : '250',
		plugins: [
			'advlist autolink lists link image charmap print preview anchor',
			'searchreplace visualblocks code fullscreen',
			'insertdatetime media table contextmenu paste code'
		],
		toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
	});
</script>
<div class="row">
	<div class="col-12">
<h2 class="head"><?php echo htmlspecialchars(Flux::message('CMSNewsHeader')) ?></h2>
<p class="sub-head">Criando uma nova Notícia.</p>
<?php if (!empty($errorMessage)): ?>
    <p class="red"><?php echo htmlspecialchars($errorMessage) ?></p>
<?php endif ?>
	</div>
</div>

<div class="row">
	<div class="col-12">
		<form action="<?php echo $this->urlWithQs ?>" method="post" class="form-main mt-4">
			<div class="row">
				<div class="col-12 col-sm-4 col-md-4">
					<div class="form-group">
						<label for="news_title"><?php echo htmlspecialchars(Flux::message('CMSNewsTitleLabel')) ?></label>
						<input type="text" class="form-control" name="news_title" id="news_title" value="<?php echo htmlspecialchars($title) ?>" required>
					</div>
				</div>
				<div class="col-12 col-sm-4 col-md-4">
					<div class="form-group">
						<label for="news_author"><?php echo htmlspecialchars(Flux::message('CMSNewsAuthorLabel')) ?></label>
						<input type="text" class="form-control" name="news_author" id="news_author" value="<?php echo htmlspecialchars($author) ?>" required>
					</div>
				</div>
				
				<div class="col-12 col-sm-4 col-md-4">
					<div class="form-group">
						<label for="news_type"><?php echo htmlspecialchars('Tipo da Notícia') ?></label>
						<select class="form-control" id="news_type" name="news_type">
							<?php foreach( $Creative->Cfg['News']['type'] as $lid => $value ): ?>
							<option value="<?php echo $lid ?>"<?php $lid == $ntype ?' selected':'' ?>><?php echo utf8_encode($value['label']) ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="form-group">
						<label for="news_body"><?php echo htmlspecialchars(Flux::message('CMSNewsBodyLabel')) ?></label>
						<textarea name="news_body" id="news_body" cols="120" class="form-control"><?php echo htmlspecialchars($body) ?></textarea>
					</div>
				</div>
				
				<div class="col-12">
					<div class="form-group">
						<label for="news_link"><?php echo htmlspecialchars(Flux::message('CMSNewsLinkLabel')) ?></label>
						<input type="text" class="form-control" name="news_link" id="news_link" value="<?php echo htmlspecialchars($link) ?>">
						<small class="form-text text-muted"><?php echo htmlspecialchars(Flux::message('CMSOptionalLabel')) ?></small>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="form-group">
						<button type="submit" value="true" class="btn btn-primary">Atualizar</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>