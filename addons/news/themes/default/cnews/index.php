<?php 
if (!defined('FLUX_ROOT')) exit;
?>
	<div class="row">
		<div class="col-12">
			<h2 class="head"><?php echo htmlspecialchars(Flux::message('CMSNewsHeader')) ?></h2>
			<p class="sub-head">Últimas Notícias do Servidor.</p>
			
			<div class="search-toggle-btn">
				<a href="#" data-target="#formSearch"><i class="fas fa-search"></i> Procurar Notícia...</a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<form class="search-toggle" method="get" id="formSearch">
				<?php echo $this->moduleActionFormInputs('cnews', 'index') ?>
				<div class="row">
					<div class="col-12 col-sm-6 col-md-4">
						<div class="form-group">
							<label for="date-init">No Período de:</label>
							<input type="date" class="form-control" id="date-init" name="date-init">
						</div>
					</div>
					<div class="col-12 col-sm-6 col-md-4">
						<div class="form-group">
							<label for="date-end">Até o Período:</label>
							<input type="date" class="form-control" id="date-end" name="date-end">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12 col-sm-6 col-md-4">
						<div class="form-group">
							<label for="desc">Descrição:</label>
							<input type="text" class="form-control" id="desc" name="desc" placeholder="Digite parcialmente o título ou conteúdo...">
						</div>
					</div>
					<div class="col-12 col-sm-6 col-md-4">
						<div class="form-group">
							<label for="category">Categoria:</label>
							<select class="form-control" id="category" name="category">
								<option value="">Todas</option>
							<?php foreach( $Creative->Cfg['News']['type'] as $id => $value ): ?>
								<option value="<?php echo $id ?>"><?php echo utf8_encode($value['label']) ?></option>
							<?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="form-group">
							<button type="submit" class="btn btn-primary"><i class="fas fa-search"></i> Procurar</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>

	<?php if($news): ?>
		<?php foreach($news as $nrow):?>
		<?php
			$date = date("d-m-Y H:i:s",strtotime($nrow->created));
			list($day,$month,$year,$hour,$min,$sec) = sscanf($date, "%d-%d-%d %d:%d:%d");
			$label = isset($Creative->Cfg['News']['type'][$nrow->type]['label']) ? $Creative->Cfg['News']['type'][$nrow->type]['label'] : null;
		?>
		<div class="row news-list">
			<div class="col-12">
				<div class="head">
					<h2><?php echo $nrow->title ?></h2>
					<p><small>Por <strong><?php echo $nrow->author ?></strong> em <strong><?php echo sprintf("%02d de %s de %d", $day, $Creative->Cfg['Months']['full'][$month], $year) ?></strong></small>
					<?php if( $label ): ?>
					<p class="tag"><a href="<?php echo $this->url('cnews','index', array('category' => $nrow->type)); ?>"><i class="fas fa-tag"></i> <?php echo utf8_encode($label) ?></a>
					<?php endif; ?>
				</div>
				<div class="content">
					<?php echo $nrow->body ?>
					<div class="clearfix"></div>
				</div>
				<div class="footer">
					<?php if($nrow->created != $nrow->modified && Flux::config('CMSDisplayModifiedBy')):?>
						<small><?php echo htmlspecialchars(Flux::message('CMSModifiedLabel')) ?> : <?php echo date('m-d-y',strtotime($nrow->modified))?></small>
					<?php endif; ?>
					<a href="<?php echo $this->url('cnews','view', array('id' => $nrow->id)); ?>"><i class="fab fa-readme"></i> Ler Notícia</a>
					<?php if($nrow->link): ?>
					| <a href="<?php echo $nrow->link ?>"><i class="fas fa-link"></i> Link Externo</a>
					<?php endif; ?>
					<?php if( $auth->actionAllowed('cnews', 'edit') ): ?>
					| <a href="<?php echo $this->url('cnews','edit', array('id' => $nrow->id)); ?>"><i class="fas fa-edit"></i> Editar</a>
					<?php endif; ?>
					<?php if( $auth->actionAllowed('cnews', 'delete') ): ?>
					| <a href="<?php echo $this->url('cnews','delete', array('id' => $nrow->id)); ?>" onclick="return confirm('<?php echo htmlspecialchars(Flux::message('CMSConfirmDeleteLabel')) ?>');"><i class="fas fa-trash-alt"></i> Excluír</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<?php endforeach; ?> 
		<?php echo $paginator->getHTML() ?>
	<?php else: ?>
		<div class="row">
			<div class="col-12">
				<p>
					Nenhum notícia encontrada...<br/><br/>
				</p>
			</div>
		</div>
	<?php endif ?>
