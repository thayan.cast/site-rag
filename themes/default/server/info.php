<?php if (!defined('FLUX_ROOT')) exit; ?>
<br>
<div style="padding-left: 10px;" class="row">
	<div class="col-md-6">
		<div class="title-wrapper">
		<h3 class="widget-title">
			Informações básicas
		</h3>
		</div>	
		<div class="wcontainer">
			<ul style="margin-bottom: 0; padding-left: 15px;">
				<li>Episodio 13.2 - Encontro com o Desconhecido.</li>
				<li>Mecânica Pré-Renovação. (Sem 3rd Classes).</li>
				<li>Sem customização de monstros e skills.</li>
				<li>Itens God desabilitados.</li>
				<li>Dual Login liberado.</li>
				<li>Servidor protegido com Gepard Shield 2.0 + Adelays.</li>
				<li>Sistemas de Batalhas Campais eAmod.</li>
				<li>Rates Flutuantes aos finais de semana</li>
			</ul>
		</div>	
		<div class="title-wrapper">
			<h3 class="widget-title">
				Sistemas Personalizados
			</h3>
		</div>
		<div class="wcontainer">
			<ul style="margin-bottom: 0; padding-left: 10px;">
				<li>Mestra das Classes</li>
				<li>Resetadora (Requer 1 neuralizador e uma quantidade de zeny por reset).</li>
				<li>Garota Negociantes.</li>
				<li>Estilista</li>
				<li>Cuarandeira por quest.</li>
				<li>Sistema de Banco (CTRL+B).</li>
				<li>Sistema de Quest. (Visitar a cidade de Alfheim)</li>
				<li>Sistema de Consumiveis BG</li>
				<li>Removedora de Cartas</li>
				<li>Death Arena Rankeada</li>
				<li>Mais de 9 Eventos automáticos</li>
				<li>Quartel General</li>
				<li>Teleportadora em todas as cidades</li>
				<li>...</li>
			</ul>
		</div>	
<div class="title-wrapper">
			<h3 class="widget-title">
				Estatísticas do servidor
			</h3>
		</div>
		<div class="wcontainer">
			<table>
				<tbody><tr>
					<th><label>Contas</label></th>
					<td width="10"></td>
					<td><button class="btn btn-xs btn-primary"><?php echo number_format($info['accounts']) ?></button></td>
				</tr>
				<tr>
					<td height="5" colspan="3"></td>
				</tr>
				<tr>
					<th><label>Personagens</label></th>
					<td width="10"></td>
					<td><button class="btn btn-xs btn-primary"><?php echo number_format($info['characters']) ?></button></td>
				</tr>
				<tr>
					<td height="5" colspan="3"></td>
				</tr>
				<tr>
					<th><label>Clãs</label></th>
					<td width="10"></td>
					<td><button class="btn btn-xs btn-primary"><?php echo number_format($info['guilds']) ?></button></td>
				</tr>
				<tr>
					<td height="5" colspan="3"></td>
				</tr>
				<tr>
					<th><label>Grupos</label></th>
					<td width="10"></td>
					<td><button class="btn btn-xs btn-primary"><?php echo number_format($info['parties']) ?></button></td>
				</tr>
				<!--
				<tr>
					<td colspan="3" height="5"></td>
				</tr>
				<tr>
					<th><label>Zeny</label></th>
					<td width="10"></td>
					<td><button class="btn btn-xs btn-primary"><?php echo number_format($info['zeny']) ?></button></td>
				</tr>
				-->
			</tbody></table>
		</div>		
	</div>
	
	<div class="col-md-6">
		<div class="title-wrapper">
			<h3 class="widget-title">
				Rates do Servidor
			</h3>
		</div>	
		<div class="wcontainer">
			<ul style="margin-bottom: 0; padding-left: 10px;">
				<li>Experiência de Base/Classe: <span class="text-success">30x ~ 60x</span> Flutuantes</li>
				<li>Experiência de Quest: <span class="text-success">1x</span></li>
				<li>Drop de Equipamentos Normais: <span class="text-success">10x</li>
				<li>Drop de Itens Usaveis/Etc: <span class="text-success">10x</span></li>
				<li>Drop de Cartas Normais:: <span class="text-success">0,25%</span></li>
				<li>Drop de Equipamentos MVP:: <span class="text-success">30%</span></li>
				<li>Drop de Cartas MVP: <span class="text-success">0,01%</span></li>
				<li>Level Máx de Base: <span class="text-success">99</span> (Trans Class)</li>
				<li>Level Máx de Classe: <span class="text-success">70</span> (Trans Class)</li>
			</ul>
		</div>	
		<div class="title-wrapper">
			<h3 class="widget-title">
				Modificações no Jogo
			</h3>
		</div>
		<div class="wcontainer">
			<ul style="margin-bottom: 0; padding-left: 10px;">
				<li>Aliança de Clãs: Habilitados.</li>
				<li>Máximo de Membros no Clã: 36.</li>
				<li>Máximo de Membros no Grupo: 15.</li>
				<li>Bnificação de Experiência por Membro no Grupo: +20%.</li>
				<li>Numero de doações para acesso ao Santuário de Rachel: 1000 doações.</li>
				<li>Battleground KVM: Desabilitada.</li>
				<li>Instinto de Defesa: Desabilitado em MVP.</li>
				<li>Abracadabra: Alterado Lista de MVP's invocados.</li>
				<li>Consumo de Zeny em Batalhas Campais: Criado Zeny Falso</li>
				<li>Prontera: Renewal</li>
				<li>...</li>
			</ul>
		</div>
		<div class="title-wrapper">
			<h3 class="widget-title">
				Informações de Classes
			</h3>
		</div>
		<div class="wcontainer">
			<?php $i = 1; $x = 5 ?>
			<?php foreach ($info['classes'] as $class => $total): ?>
			<div class="form-group">
				<div class="btn-group btn-group-justified">
					<div class="btn-group">
						<button class="btn btn-xs btn-primary" type="button"><?php echo htmlspecialchars($class) ?></button>
					</div>
					<div class="btn-group">
						<button class="btn btn-xs btn-info" type="button"><?php echo number_format($total) ?></button>
					</div>

				</div>
			</div>
			<?php if ($i++ % $x === 0): ?>
			<?php endif ?>
			<?php endforeach ?>
		</div>		
	</div>
</div>