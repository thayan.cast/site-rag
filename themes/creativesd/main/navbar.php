<?php if (!defined('FLUX_ROOT')) exit; ?>

<?php
	// Server
	$serverNames = $this->getServerNames();
	
	if( ($serverInfo = $Creative->ReadCache($Creative->Cfg['Server']['cache'])) == null ) {
		$serverInfo = $Creative->RequestInfo($Creative->Cfg['Server']['login']['ip'], $Creative->Cfg['Server']['login']['port'], $Creative->Cfg['Server']['char']['ip'], $Creative->Cfg['Server']['char']['port'], $Creative->Cfg['Server']['map']['ip'], $Creative->Cfg['Server']['map']['port']);
		$Creative->SaveCache($Creative->Cfg['Server']['cache'],time()+$Creative->Cfg['Server']['expire'],$serverInfo);
	}
	
	// Peak
	$peak = $Creative->getPeak();
	
	// Server Info
	$serverOn = $warOn = $usersOn = $countVend = 0;
	if( $serverInfo['map'] && $serverInfo['char'] && $serverInfo['login'] ) {
		// Server Online
		$serverOn = true;
		
		// Users Online
		$usersOn = $Creative->getPlayersOn();
		
		// War of Emperium
		$warOn = $Creative->getWarOn();
		
		// Vendings
		//$countVend = $Creative->countVend();
	}
	
	//$countAcc = $Creative->countAccount();
	//$countChars = $Creative->countCharacters();
	//$countGuild = $Creative->countGuild();
	//$countParty = $Creative->countParty();
	
	// Template
	$menuIcon = $this->themePath('images/icon/menu.png');
?>
	<div class="menu-bar">
		<div class="container">
			<div class="server-status-wrapper">
				<div class="server-status invisible" data-animation="fadeIn">
					<div class="items">
						<div class="item">
							<div class="icon" data-toggle="tooltip-xs" data-html="true" title="<strong>Servidor:</strong> <?php echo $serverOn ? "On": "Off" ?>">
								<i class="fas fa-server"></i>
							</div>
							<div class="base auto <?php echo $serverOn ? 'active' : 'deactivate' ?>">
								<?php echo $serverOn ? 'On' : 'Off' ?>-Line
							</div>
						</div>
						<div class="item">
							<div class="icon" data-toggle="tooltip-xs" data-html="true" title="<strong>Jogadores Onlines:</strong> <?php echo $usersOn ?>">
								<i class="fas fa-globe-americas"></i>
							</div>
							<div class="base auto">
								<?php echo number_format($usersOn) ?> Jogadores
							</div>
						</div>
						<div class="item">
							<div class="icon" data-toggle="tooltip-xs" data-html="true" title="<strong>Pico de Jogadores:</strong> <?php echo $peak ?>">
								<i class="fas fa-fire"></i>
							</div>
							<div class="base auto">
								<?php echo number_format($peak) ?> Jogadores
							</div>
						</div>
						<div class="item">
							<div class="icon" data-toggle="tooltip-xs" data-html="true" title="<strong>Guerra do Emperium:</strong> <?php echo $warOn ? "On":"Off" ?>">
								<i class="fas fa-chess-rook"></i>
							</div>
							<div class="base auto <?php echo $warOn ? 'active' : 'deactivate' ?>">
								<?php echo $warOn ? 'On' : 'Off' ?>-Line
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<nav class="navbar navbar-expand-xl navbar-custom invisible">
			  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainMenu" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation" data-animation="fadeIn">
				<i class="fas fa-bars"></i> Menu
			  </button>
			  <div class="collapse navbar-collapse" data-animation="fadeIn" id="mainMenu">
					<ul class="navbar-nav ml-auto">
					   <li class="nav-item active">
						<a class="nav-link" href="<?php echo $this->url('main','index'); ?>">
							<i class="fas fa-home"></i> Home
						</a>
					   </li>
					   <li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fas fa-info"></i> Informações
						</a>
						<div class="dropdown-menu dropdown-menu-left animate slideIn">
						  <div class="head">
							<h2>Informações</h2>
							<p>Fique por dentro de tudo do nosso servidor.</p>
						   </div>
						  <a class="dropdown-item" href="<?php echo $this->url('cnews'); ?>"><img src="<?php echo $menuIcon ?>" class="menu-icon" /> Notícias</a>
						  <a class="dropdown-item" href="<?php echo $this->url('server','info'); ?>"><img src="<?php echo $menuIcon ?>" class="menu-icon" /> Servidor</a>
						  <a class="dropdown-item" href="<?php echo $this->url('donate','main'); ?>"><img src="<?php echo $menuIcon ?>" class="menu-icon" /> Doações</a>
						  <a class="dropdown-item" href="<?php echo $this->url('voteforpoints'); ?>"><img src="<?php echo $menuIcon ?>" class="menu-icon" /> Vote por Pontos</a>
						  <a class="dropdown-item" href="<?php echo $this->url('woe'); ?>"><img src="<?php echo $menuIcon ?>" class="menu-icon" /> Guerra do Emperium</a>
						  <a class="dropdown-item" href="<?php echo $this->url('pages','content', array('path' => 'rules')); ?>"><img src="<?php echo $menuIcon ?>" class="menu-icon" /> Regras</a>
						</div>
					  </li>
					  <li class="nav-item">
						<a class="nav-link" href="<?php echo $this->url('pages','content', array('path' => 'downloads')); ?>">
							<i class="fas fa-download"></i> Downloads
						</a>
					  </li>
					  <li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fas fa-trophy"></i> Ranking
						</a>
						<div class="dropdown-menu dropdown-menu-left animate slideIn">
						  <div class="head">
							<h2>Informações</h2>
							<p>Saiba quem são os Tops Clãs e Jogadores do Servidor.</p>
						   </div>
						  <a class="dropdown-item" href="<?php echo $this->url('ranking','alchemist'); ?>"><img src="<?php echo $menuIcon ?>" class="menu-icon" /> Alquimia</a>
						  <a class="dropdown-item" href="<?php echo $this->url('ranking','break'); ?>"><img src="<?php echo $menuIcon ?>" class="menu-icon" /> Emperium Break</a>
						  <a class="dropdown-item" href="<?php echo $this->url('ranking','blacksmith'); ?>"><img src="<?php echo $menuIcon ?>" class="menu-icon" /> Forja</a>
						  <a class="dropdown-item" href="<?php echo $this->url('ranking','woe'); ?>"><img src="<?php echo $menuIcon ?>" class="menu-icon" /> Guerra do Emperium</a>
						  <a class="dropdown-item" href="<?php echo $this->url('ranking','homunculus'); ?>"><img src="<?php echo $menuIcon ?>" class="menu-icon" /> Homunculus</a>
						  <a class="dropdown-item" href="<?php echo $this->url('ranking', 'gvg'); ?>"><img src="<?php echo $menuIcon ?>" class="menu-icon" /> Top GvG</a>
						  <a class="dropdown-item" href="<?php echo $this->url('ranking', 'ggvg'); ?>"><img src="<?php echo $menuIcon ?>" class="menu-icon" /> Top GvG Clã</a>
						  <a class="dropdown-item" href="<?php echo $this->url('ranking', 'pvp'); ?>"><img src="<?php echo $menuIcon ?>" class="menu-icon" /> Top PvP</a>
						  <a class="dropdown-item" href="<?php echo $this->url('ranking','mvp2'); ?>"><img src="<?php echo $menuIcon ?>" class="menu-icon" /> Top MvP</a>
						  <a class="dropdown-item" href="<?php echo $this->url('ranking','zeny'); ?>"><img src="<?php echo $menuIcon ?>" class="menu-icon" /> Zeny</a>
						</div>
					  </li>
					  <li class="nav-item">
						<a class="nav-link d-lg-block d-xl-none" href="<?php echo $Creative->Cfg['Social']['board'] ?>" target="_blank">
							<i class="fas fa-star"></i> Fórum
						</a>
					  </li>
					  <?php
						if( $session->isLoggedIn() ):
							$adminMenuItems = $this->getAdminMenuItems();
							if( count($adminMenuItems) ):
					  ?>
					   <li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						  <i class="fas fa-user-cog"></i> Admin CP
						</a>
						<div class="dropdown-menu dropdown-menu-left animate slideIn">
						   <div class="head">
							<h2>AdminCP</h2>
							<p>Opções do Administrador.</p>
						   </div>
						<?php
								foreach ($adminMenuItems as $menuCategory => $menus):
						?>
							 <a class="dropdown-item" href="<?php echo $menus['url'] ?>"><img src="<?php echo $menuIcon ?>" class="menu-icon" /> <?php echo Flux::message($menus['name']) ?></a>
						<?php
								endforeach;
						?>
						</div>
					  </li>
						
				<?php
						endif;
					endif;
				?>
					  <li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						  <i class="fas fa-user"></i> Painel de Controle
						</a>
						<div class="dropdown-menu dropdown-menu-right animate slideIn">
						<?php if ($session->isLoggedIn() === false): ?>
						  <form class="px-4 py-3" id="quick-login">
						<?php if (count($serverNames) === 1): ?>
							<input type="hidden" name="server" value="<?php echo htmlspecialchars($session->loginAthenaGroup->serverName) ?>">
						<?php endif; ?>
							<div class="head">
								<h2>Entrar</h2>
								<p>Conecte-se ao nosso painel de controle.</p>
							</div>
							<div class="form-group">
								 <label for="quserid">Usuário:</label>
								 <input type="text" name="username" class="form-control" id="quserid" placeholder="Entre com seu usuário...">
							</div>
							<div class="form-group">
								<label for="quserpass">Senha:</label>
								<input type="password" name="password" class="form-control" id="quserpass" placeholder="Entre com sua senha...">
							</div>
							<?php if (count($serverNames) > 1): ?>
							<div class="form-group">
								<label for="qserver"><?php echo utf8_encode('Servidor:') ?></label>
								<select name="server" class="form-control" id="qserver">
							<?php foreach ($serverNames as $serverName): ?>
									<option value="<?php echo htmlspecialchars($serverName) ?>"><?php echo htmlspecialchars($serverName) ?></option>
							<?php endforeach ?>
								</select>
							</div>
							<?php endif ?>
							<button type="submit" class="btn btn-primary">Entrar</button>
							<div class="links">
								<a href="<?php echo $this->url('account','create'); ?>" class="float-left">Criar Conta</a>
								<a href="<?php echo $this->url('account','resetpass'); ?>" class="float-right">Recuperar Senha</a>
							</div>
						  </form>
						<?php
							else:
								$menuItems = $this->getMenuItems();
								if (!empty($menuItems['AccountLabel'])):
						?>
						   <div class="head">
							<h2>Painel de Controle</h2>
							<p>Gerencie sua conta em nosso Painel Integrado.</p>
						   </div>
						<?php
									foreach ($menuItems['AccountLabel'] as $menuCategory => $menus):
						?>
						   <a class="dropdown-item" href="<?php echo $menus['url'] ?>"><img src="<?php echo $menuIcon ?>" class="menu-icon" /> <?php echo Flux::message($menus['name']) ?></a>
						<?php
									endforeach;
								endif;
							endif;
						?>
						</div>
					  </li>
					</ul>
				</div>
			</nav>
		</div>
	</div>
	
	<div id="fog"></div>
	<div class="header<?php if( $params->get('module') != 'main' ) { echo " compact"; } ?>">
		<div class="container">
			<div class="logo invisible" data-animation="fadeIn">
				<a href="<?php echo $this->url('main','index'); ?>" data-animation="fadeIn"><img class="animated" src="<?php echo $this->themePath('images/logo.png') ?>"></a>
			</div>
		</div>
	</div>
	
	<div class="top-bar">
		<div class="container">
			<div class="quick-link">
				<div class="items">
					<a href="http://SEU_RAG.com.br/?module=voteforpoints" class="item green d-none d-lg-inline">
						<div class="base">
							<h2>Vote</h2>
							<p>Por Pontos</p>
						</div>
					</a>
					<a href="http://www.SEU_RAG.com.br/?module=donate&action=main" class="item blue d-none d-lg-inline">
						<div class="base">
							<h2>Doações</h2>
							<p>Ajude o Servidor!</p>
						</div>
					</a>
					<a href="http://forum.SEU_RAG.com.br/" class="item orange d-none d-lg-none d-xl-inline">
						<div class="base">
							<h2>Fórum</h2>
							<p>Comunidade de Suporte</p>
						</div>
					</a>
				</div>
			</div>
			<div class="community-wrapper">
				<div class="community-links invisible" data-animation="fadeIn">
					<a href="<?php echo $Creative->Cfg['Social']['instagram'] ?>" target="_blank" class="community-link" data-animation="fadeIn"><i class="fab fa-instagram"></i></a>
					<a href="<?php echo $Creative->Cfg['Social']['youtube'] ?>" target="_blank" class="community-link" data-animation="fadeIn"><i class="fab fa-youtube"></i></a>
					<a href="<?php echo $Creative->Cfg['Social']['discord'] ?>" target="_blank" class="community-link" data-animation="fadeIn"><i class="fab fa-discord"></i></a>
					<a href="<?php echo $Creative->Cfg['Social']['facebook'] ?>" target="_blank" class="community-link" data-animation="fadeIn"><i class="fab fa-facebook"></i></a>
				</div>
				<div class="community-bar invisible">
					<form class="search-database" action="?" method="get" data-animation="fadeIn">
						<input type="hidden" name="action" value="index">
						<input type="text" name="name" placeholder="Procurar...">
						<div class="dropdown">
							<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownSearchBtn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Item
							</button>
							<div class="dropdown-menu" aria-labelledby="dropdownSearchBtn">
								<a class="dropdown-item" href="#" data-value="item">Item</a>
								<a class="dropdown-item" href="#" data-value="mob">Monstro</a>
							 </div>
						</div>
						<input type="hidden" name="module" value="item">
						<button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
					</form>
				</div>
			</div>
		</div>
	</div>