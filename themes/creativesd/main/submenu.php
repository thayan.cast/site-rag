<?php if (!defined('FLUX_ROOT')) exit; ?>
<?php $subMenuItems = $this->getSubMenuItems(); $menus = array() ?>
<?php if( $params->get('module') != 'main' ): ?>
	<div class="sub-menu">
		<div class="container">
			<div class="menu-icon">
				<i class="fas fa-bars"></i> Menu:
			</div>
			<ol class="breadcrumb">
<?php if (!empty($subMenuItems)): ?>
	<?php foreach ($subMenuItems as $menuItem): ?>
		<?php $menus[] = sprintf('<li class="breadcrumb-item"><a href="%s" class="sub-menu-item%s">%s</a></li>',
			$this->url($menuItem['module'], $menuItem['action']),
			$params->get('module') == $menuItem['module'] && $params->get('action') == $menuItem['action'] ? ' current-sub-menu' : '',
			htmlspecialchars($menuItem['name'])) ?>
	<?php endforeach ?>
<?php else: ?>
<?php $menus[] = sprintf('<li class="breadcrumb-item"><a href="%s%s" class="sub-menu-item current-sub-menu">%s</a></li>',
			$this->url,
			$params->get('path') ? '&path='.$params->get('path') : '',
			htmlspecialchars($title)) ?>
<?php endif ?>
	<?php echo implode('', $menus) ?>
			</ol>
		</div>
	</div>
<?php endif ?>
