﻿<?php if (!defined('FLUX_ROOT')) exit; 

	// Notícias
	$news = $Creative->getNews();
	
	// Status
	$serverNames = $this->getServerNames();
	
	// Rankings
	// PvP
	$pvpdata = $Creative->getPvPRank(0);
	// GvG
	$pgvgdata = $Creative->getPvPRank(1);
	// Guild GvG
	$ggvgdata = $Creative->getGvGRank();
	// Guild Break
	$gbdata = $Creative->getGBRank();
	// Player Break
	$pbdata = $Creative->getPBRank();
	// MvP Rank
	$mvpdata = $Creative->getMvPRank();
	// Castle Data
	//$castledata = $Creative->getCastleData();
	
	// CashShop
require_once 'Flux/TemporaryTable.php';
	$cashshop = $Creative->getCashShop();
?>
	
		<div class="container home">
			<div class="row">
				<div class="col-12">
					<div class="news invisible" data-animation="fadeIn">
						<div class="head-wrapper">
							<div class="title">
								<div class="icon">
									<img src="<?php echo $this->themePath('images/icon/news.png') ?>" />
								</div>
								<div class="text">
									Notícias
								</div>
							</div>
							<div class="filter-wrapper">
								<div class="dropdown filter">
									<button class="btn btn-secondary dropdown-toggle" type="button" id="newsFilterButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<span>Todas</span> <i class="fas fa-chevron-down"></i>
									</button>
									<div class="dropdown-menu dropdown-menu-right" aria-labelledby="newsFilterButton">
										<a class="dropdown-item active" href="#" data-value="all">Todas</a>
										<a class="dropdown-item" href="#" data-value="0">Notícias</a>
										<a class="dropdown-item" href="#" data-value="1">Atualizações</a>
										<a class="dropdown-item" href="#" data-value="2">Eventos</a>
										<a class="dropdown-item" href="#" data-value="3">Manutenção</a>
									</div>
								</div>
								<a href="<?php echo $this->url('cnews','index') ?>" class="read-more"><i class="fas fa-plus"></i></a>
							</div>
						</div>
						<div class="items">
						<?php
							if( count($news) ):
								foreach($news as $nrow):
								if( isset($Creative->Cfg['News']['type'][$nrow->type]) ) {
									$label = $Creative->Cfg['News']['type'][$nrow->type]['label'];
									$class = $Creative->Cfg['News']['type'][$nrow->type]['class'];
								}
								else {
									$label = $Creative->Cfg['News']['type']['all']['label'];
									$class = $Creative->Cfg['News']['type']['all']['class'];
								}
						?>
								<a href="<?php echo $this->url('cnews','view',array('id' => $nrow->id)); ?>" class="item">
									<div class="tag <?php echo $class ?>"><?php echo utf8_encode($label) ?></div>
									<div class="title"><?php echo $nrow->title ?></div>
									<div class="date"><?php echo date("d.m.Y",strtotime($nrow->created)) ?></div>
								</a>
						<?php
								endforeach;
							else:
						?>
							<div class="empty">
								<h2>Ooops!</h2>
								<p>Nenhuma notícia encontrada...</p>
							</div>
						<?php
							endif
						?>
						</div>
					</div>					
					<div class="slider-container invisible" data-animation="fadeIn">
							<ul id="slider" class="anim-slider">
								<li class="anim-slide">
									<div class="content">
										<a href="#" class="description">
											<div class="head">Bem-vindo!</div>
											<div class="text">Bem-vindo aventureiro, estamos contente em ter você aqui conosco.</div>
										</a>
									</div>
								</li>
								<li class="anim-slide">
									<div class="content">
										<a href="#" class="description">
											<div class="head">Não perca tempo!</div>
											<div class="text">Participe dos nossos eventos e ganhe prêmios importantes.</div>
										</a>
									</div>
								</li>
								<li class="anim-slide">
									<div class="content">
										<a href="#" class="description">
											<div class="head">Faça parte!</div>
											<div class="text">Participe do nosso fórum de suporte, eventos especiais acontecem lá também.</div>
										</a>
									</div>
								</li>
								
								<!-- Arrows -->
								<nav class="anim-arrows">
									<span class="anim-arrows-prev"></span>
									<span class="anim-arrows-next"></span>
								</nav>
							</ul>
						</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="wrapper-main">
						<div class="wrapper-left">
							<div class="face-wrapper invisible" data-animation="fadeIn">
								<div id="fb-root"></div>
								<script async defer crossorigin="anonymous" src="https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v5.0"></script>
								<div class="fb-page" data-href="https://www.facebook.com/RagnarokWPBR/" data-tabs="timeline" data-width="800" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/">&nbsp;</a></blockquote></div>
							</div>
						</div>
						<div class="wrapper-center">
							<div class="ranking invisible" data-animation="fadeIn">
								<div class="content">
									<div class="head-wrapper">
										<div class="title">
											<div class="icon">
												<img src="<?php echo $this->themePath('images/icon/ranking.png') ?>" />
											</div>
											<div class="text">
												Ranking
											</div>
										</div>
										<div class="filter-wrapper">
											<div class="dropdown filter">
												<button class="btn btn-secondary dropdown-toggle" type="button" id="rankingFilterButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													<span>Ranking PvP</span> <i class="fas fa-chevron-down"></i>
												</button>
												<div class="dropdown-menu dropdown-menu-right" aria-labelledby="rankingFilterButton">
													<a class="dropdown-item active" href="#" data-value="#pvp-ranked" data-url="<?php echo $this->url('ranking','pvp') ?>">Ranking PvP</a>
													<a class="dropdown-item" href="#" data-value="#player-gvg" data-url="<?php echo $this->url('ranking','gvg') ?>">Jogador GvG</a>
													<a class="dropdown-item" href="#" data-value="#guild-gvg" data-url="<?php echo $this->url('ranking','ggvg') ?>">Clã GvG</a>
													<a class="dropdown-item" href="#" data-value="#woe" data-url="<?php echo $this->url('ranking','woe') ?>">Guerra do Emperium</a>
													<a class="dropdown-item" href="#" data-value="#break" data-url="<?php echo $this->url('ranking','break') ?>">Top Break</a>
													<a class="dropdown-item" href="#" data-value="#mvp" data-url="<?php echo $this->url('ranking','mvp') ?>">Top MvP</a>
												</div>
											</div>
											<a href="<?php echo $this->url('ranking','pvp') ?>" class="read-more"><i class="fas fa-plus"></i></a>
										</div>
									</div>
								</div>
								<div class="content rank-wrapper" id="pvp-ranked">
									<div class="view">
									<?php
										if( isset($pvpdata[0]) ):
											$name = htmlspecialchars($pvpdata[0]['name']);
											$guild_id = $pvpdata[0]['guild_id'] ? $pvpdata[0]['guild_id'] : false;
											$guild_name = $pvpdata[0]['guild_id'] && $pvpdata[0]['guild_name'] ? htmlspecialchars($pvpdata[0]['guild_name']) : '';
											$emblem = $guild_id && $pvpdata[0]['emblem_len'] ? $this->emblem($pvpdata[0]['guild_id']) : $this->themePath('images/default-emblem.png');
											$sex = $pvpdata[0]['sex'] == 'F' ? 'f' : 'm';
											$class = isset($Creative->Cfg['JobAliasesGender'][$sex][$pvpdata[0]['job']]) ? $Creative->Cfg['JobAliasesGender'][$sex][$pvpdata[0]['job']] : $pvpdata[0]['job'];
											$icon = isset($Creative->Cfg['JobAliases'][$class]) ? $Creative->Cfg['JobAliases'][$class] : $class;
											$char_url = "?module=character&action=view&id=".$pvpdata[0]['char_id'];
											if( $guild_id )
												$guild_url = "?module=guild&action=view&id=".$guild_id;
											else
												$guild_url = "";
									?>
										<div class="name">
											<strong>1#</strong> <?php echo $name ?>
										</div>
										<div class="image">
											<img src="<?php echo $this->themePath('images/class/job_' . $icon . '_' . $sex . '.png') ?>" class="jobimg" />
											<div class="desc">
												<a href="<?php echo $char_url ?>"" data-toggle="tooltip" data-placement="top" data-html="true" title="<?php echo htmlspecialchars($this->jobClassText($class)) ?>"><img src="<?php echo $this->themePath('images/class/guild/' . $icon . '.png') ?>"></a>
											<?php if( $guild_id ): ?>
												<a href="<?php echo $guild_url ?>"" data-toggle="tooltip" data-placement="top" data-html="true" title="<?php echo htmlspecialchars($guild_name) ?>"><img src="<?php echo $emblem ?>"></a>
											<?php endif ?>
											</div>
										</div>
										<div class="more">
											<a href="<?php echo $char_url ?>" class="btn btn-success">Visualizar</a>
										</div>
									<?php endif ?>
									</div>
									<div class="positions">
										<table class="table">
											<thead>
												<tr>
													<th class="text-center">#</th>
													<th>Nome</th>
													<th class="text-center">K</th>
													<th class="text-center">D</th>
													<th class="text-center">R</th>
												</tr>
											</thead>
											<tbody>
											<?php
												for( $i=0; $i < $Creative->Cfg['rank_home_rows']; $i++ ):
													if( isset($pvpdata[$i]) ) :
														$name = htmlspecialchars($pvpdata[$i]['name']);
														$guild_id = $pvpdata[$i]['guild_id'] ? $pvpdata[$i]['guild_id'] : 0;
														$guild_name = $pvpdata[$i]['guild_name'] ? htmlspecialchars($pvpdata[$i]['guild_name']) : '';
														$emblem = $guild_id && $pvpdata[$i]['emblem_len'] ? $this->emblem($pvpdata[$i]['guild_id']) : $this->themePath('images/default-emblem.png');
														$sex = $pvpdata[$i]['sex'] == 'F' ? 'f' : 'm';
														$class = isset($Creative->Cfg['JobAliasesGender'][$sex][$pvpdata[$i]['job']]) ? $Creative->Cfg['JobAliasesGender'][$sex][$pvpdata[$i]['job']] : $pvpdata[$i]['job'];
														$icon = isset($Creative->Cfg['JobAliases'][$class]) ? $Creative->Cfg['JobAliases'][$class] : $class;
														$char_url = "?module=character&action=view&id=".$pvpdata[$i]['char_id'];
														if( $guild_id )
															$guild_url = "?module=guild&action=view&id=".$guild_id;
														else
															$guild_url = "";
											?>
											
												<tr data-action="rank-change" data-type="char" data-target="#pvp-ranked" data-pos="<?php echo ($i+1) ?>" data-charname="<?php echo $name ?>" data-guildname="<?php echo $guild_name ?>" data-guildid="<?php echo $guild_id ?>" data-guildemblem="<?php echo $emblem ?>" data-jobname="<?php echo htmlspecialchars($this->jobClassText($class)) ?>" data-jobicon="<?php echo $this->themePath('images/class/guild/' . $icon . '.png') ?>" data-jobimage="<?php echo $this->themePath('images/class/job_' . $icon . '_' . $sex . '.png') ?>" data-charurl="<?php echo $char_url ?>" data-guildurl="<?php echo $guild_url ?>">
													<td class="text-center"><?php echo ($i+1) ?></td>
													<td><?php echo $name ?></td>
													<td class="text-center"><?php echo number_format($pvpdata[$i]['kill']) ?></td>
													<td class="text-center"><?php echo number_format($pvpdata[$i]['die']) ?></td>
													<td class="text-center"><?php echo number_format($pvpdata[$i]['ration']) ?></td>
												</tr>
												<?php else: ?>
												<tr>
													<td colspan="5">&nbsp;</td>
												</tr>
												<?php endif; ?>
											<?php endfor ?>
											</tbody>
										</table>
									</div>
								</div>
								
								<div class="content rank-wrapper ranking-hidden" id="player-gvg">
									<div class="view">
									<?php
										if( isset($pgvgdata[0]) ):
											$name = htmlspecialchars($pgvgdata[0]['name']);
											$guild_id = $pgvgdata[0]['guild_id'] ? $pgvgdata[0]['guild_id'] : false;
											$guild_name = $pgvgdata[0]['guild_id'] && $pgvgdata[0]['guild_name'] ? htmlspecialchars($pgvgdata[0]['guild_name']) : '';
											$emblem = $guild_id && $pgvgdata[0]['emblem_len'] ? $this->emblem($pgvgdata[0]['guild_id']) : $this->themePath('images/default-emblem.png');
											$sex = $pgvgdata[0]['sex'] == 'F' ? 'f' : 'm';
											$class = isset($Creative->Cfg['JobAliasesGender'][$sex][$pgvgdata[0]['job']]) ? $Creative->Cfg['JobAliasesGender'][$sex][$pgvgdata[0]['job']] : $pgvgdata[0]['job'];
											$icon = isset($Creative->Cfg['JobAliases'][$class]) ? $Creative->Cfg['JobAliases'][$class] : $class;
											$char_url = "?module=character&action=view&id=".$pgvgdata[0]['char_id'];
											if( $guild_id )
												$guild_url = "?module=guild&action=view&id=".$guild_id;
											else
												$guild_url = "";
									?>
										<div class="name">
											<strong>1#</strong> <?php echo $name ?>
										</div>
										<div class="image">
											<img src="<?php echo $this->themePath('images/class/job_' . $icon . '_' . $sex . '.png') ?>" class="jobimg" />
											<div class="desc">
												<a href="<?php echo $char_url ?>"" data-toggle="tooltip" data-placement="top" data-html="true" title="<?php echo htmlspecialchars($this->jobClassText($class)) ?>"><img src="<?php echo $this->themePath('images/class/guild/' . $icon . '.png') ?>"></a>
											<?php if( $guild_id ): ?>
												<a href="<?php echo $guild_url ?>"" data-toggle="tooltip" data-placement="top" data-html="true" title="<?php echo htmlspecialchars($guild_name) ?>"><img src="<?php echo $emblem ?>"></a>
											<?php endif ?>
											</div>
										</div>
										<div class="more">
											<a href="<?php echo $char_url ?>" class="btn btn-success">Visualizar</a>
										</div>
									<?php endif ?>
									</div>
									<div class="positions">
										<table class="table">
											<thead>
												<tr>
													<th class="text-center">#</th>
													<th>Nome</th>
													<th class="text-center">K</th>
													<th class="text-center">D</th>
													<th class="text-center">R</th>
												</tr>
											</thead>
											<tbody>
											<?php
												for( $i=0; $i < $Creative->Cfg['rank_home_rows']; $i++ ):
													if( isset($pgvgdata[$i]) ) :
														$name = htmlspecialchars($pgvgdata[$i]['name']);
														$guild_id = $pgvgdata[$i]['guild_id'] ? $pgvgdata[$i]['guild_id'] : 0;
														$guild_name = $pgvgdata[$i]['guild_name'] ? htmlspecialchars($pgvgdata[$i]['guild_name']) : '';
														$emblem = $guild_id && $pgvgdata[$i]['emblem_len'] ? $this->emblem($pgvgdata[$i]['guild_id']) : $this->themePath('images/default-emblem.png');
														$sex = $pgvgdata[$i]['sex'] == 'F' ? 'f' : 'm';
														$class = isset($Creative->Cfg['JobAliasesGender'][$sex][$pgvgdata[$i]['job']]) ? $Creative->Cfg['JobAliasesGender'][$sex][$pgvgdata[$i]['job']] : $pgvgdata[$i]['job'];
														$icon = isset($Creative->Cfg['JobAliases'][$class]) ? $Creative->Cfg['JobAliases'][$class] : $class;
														$char_url = "?module=character&action=view&id=".$pgvgdata[$i]['char_id'];
														if( $guild_id )
															$guild_url = "?module=guild&action=view&id=".$guild_id;
														else
															$guild_url = "";
											?>
											
												<tr data-action="rank-change" data-type="char" data-target="#player-gvg" data-pos="<?php echo ($i+1) ?>" data-charname="<?php echo $name ?>" data-guildname="<?php echo $guild_name ?>" data-guildid="<?php echo $guild_id ?>" data-guildemblem="<?php echo $emblem ?>" data-jobname="<?php echo htmlspecialchars($this->jobClassText($class)) ?>" data-jobicon="<?php echo $this->themePath('images/class/guild/' . $icon . '.png') ?>" data-jobimage="<?php echo $this->themePath('images/class/job_' . $icon . '_' . $sex . '.png') ?>" data-charurl="<?php echo $char_url ?>" data-guildurl="<?php echo $guild_url ?>">
													<td class="text-center"><?php echo ($i+1) ?></td>
													<td><?php echo $name ?></td>
													<td class="text-center"><?php echo number_format($pgvgdata[$i]['kill']) ?></td>
													<td class="text-center"><?php echo number_format($pgvgdata[$i]['die']) ?></td>
													<td class="text-center"><?php echo number_format($pgvgdata[$i]['ration']) ?></td>
												</tr>
												<?php else: ?>
												<tr>
													<td colspan="5">&nbsp;</td>
												</tr>
												<?php endif; ?>
											<?php endfor ?>
											</tbody>
										</table>
									</div>
								</div>
								
								<div class="content rank-wrapper ranking-hidden" id="guild-gvg">
									<div class="view">
									<?php
										if( isset($ggvgdata[0]) ):
											$name = htmlspecialchars($ggvgdata[0]['name']);
											$guild_id = $ggvgdata[0]['guild_id'];
											$emblem = $guild_id && $ggvgdata[0]['emblem_len'] ? $this->emblem($ggvgdata[0]['guild_id']) : $this->themePath('images/default-emblem.png');
											$guild_url = "?module=guild&action=view&id=".$guild_id;
									?>
										<div class="name">
											<strong>1#</strong> <?php echo $name ?>
										</div>
										<div class="image guild-flag">
											<img src="<?php echo $emblem ?>" class="guildimg" />
										</div>
										<div class="more">
											<a href="<?php echo $guild_url ?>" class="btn btn-success">Visualizar</a>
										</div>
									<?php endif ?>
									</div>
									<div class="positions">
										<table class="table">
											<thead>
												<tr>
													<th class="text-center">#</th>
													<th>Nome</th>
													<th class="text-center">K</th>
													<th class="text-center">D</th>
													<th class="text-center">R</th>
												</tr>
											</thead>
											<tbody>
											<?php
												for( $i=0; $i < $Creative->Cfg['rank_home_rows']; $i++ ):
													if( isset($ggvgdata[$i]) ) :
														$name = htmlspecialchars($ggvgdata[$i]['name']);
														$guild_id = $ggvgdata[$i]['guild_id'];
														$emblem = $guild_id && $ggvgdata[$i]['emblem_len'] ? $this->emblem($ggvgdata[$i]['guild_id']) : $this->themePath('images/default-emblem.png');
														$guild_url = "?module=guild&action=view&id=".$guild_id;
											?>
											
												<tr data-action="rank-change" data-type="guild" data-target="#guild-gvg" data-pos="<?php echo ($i+1) ?>" data-guildname="<?php echo $name ?>" data-guildid="<?php echo $guild_id ?>" data-guildemblem="<?php echo $emblem ?>" data-guildurl="<?php echo $guild_url ?>">
													<td class="text-center"><?php echo ($i+1) ?></td>
													<td><?php echo $name ?></td>
													<td class="text-center"><?php echo number_format($ggvgdata[$i]['kill']) ?></td>
													<td class="text-center"><?php echo number_format($ggvgdata[$i]['die']) ?></td>
													<td class="text-center"><?php echo number_format($ggvgdata[$i]['ration']) ?></td>
												</tr>
												<?php else: ?>
												<tr>
													<td colspan="5">&nbsp;</td>
												</tr>
												<?php endif; ?>
											<?php endfor ?>
											</tbody>
										</table>
									</div>
								</div>
								
								<div class="content rank-wrapper ranking-hidden" id="woe">
									<div class="view">
									<?php
										if( isset($gbdata[0]) ):
											$name = htmlspecialchars($gbdata[0]['name']);
											$guild_id = $gbdata[0]['guild_id'];
											$emblem = $guild_id && $gbdata[0]['emblem_len'] ? $this->emblem($gbdata[0]['guild_id']) : $this->themePath('images/default-emblem.png');
											$guild_url = "?module=guild&action=view&id=".$guild_id;
									?>
										<div class="name">
											<strong>1#</strong> <?php echo $name ?>
										</div>
										<div class="image guild-flag">
											<img src="<?php echo $emblem ?>" class="guildimg" />
										</div>
										<div class="more">
											<a href="<?php echo $guild_url ?>" class="btn btn-success">Visualizar</a>
										</div>
									<?php endif ?>
									</div>
									<div class="positions">
										<table class="table">
											<thead>
												<tr>
													<th class="text-center">#</th>
													<th>Nome</th>
													<th class="text-center">Pontos</th>
												</tr>
											</thead>
											<tbody>
											<?php
												for( $i=0; $i < $Creative->Cfg['rank_home_rows']; $i++ ):
													if( isset($gbdata[$i]) ) :
														$name = htmlspecialchars($gbdata[$i]['name']);
														$guild_id = $gbdata[$i]['guild_id'];
														$emblem = $guild_id && $gbdata[$i]['emblem_len'] ? $this->emblem($gbdata[$i]['guild_id']) : $this->themePath('images/default-emblem.png');
														$guild_url = "?module=guild&action=view&id=".$guild_id;
											?>
											
												<tr data-action="rank-change" data-type="guild" data-target="#woe" data-pos="<?php echo ($i+1) ?>" data-guildname="<?php echo $name ?>" data-guildid="<?php echo $guild_id ?>" data-guildemblem="<?php echo $emblem ?>" data-guildurl="<?php echo $guild_url ?>">
													<td class="text-center"><?php echo ($i+1) ?></td>
													<td><?php echo $name ?></td>
													<td class="text-center"><?php echo number_format($gbdata[$i]['break']) ?></td>
												</tr>
												<?php else: ?>
												<tr>
													<td colspan="3">&nbsp;</td>
												</tr>
												<?php endif; ?>
											<?php endfor ?>
											</tbody>
										</table>
									</div>
								</div>
								
								<div class="content rank-wrapper ranking-hidden" id="break">
									<div class="view">
									<?php
										if( isset($pbdata[0]) ):
											$name = htmlspecialchars($pbdata[0]['name']);
											$guild_id = $pbdata[0]['guild_id'] ? $pbdata[0]['guild_id'] : false;
											$guild_name = $pbdata[0]['guild_id'] && $pbdata[0]['guild_name'] ? htmlspecialchars($pbdata[0]['guild_name']) : '';
											$emblem = $guild_id && $pbdata[0]['emblem_len'] ? $this->emblem($pbdata[0]['guild_id']) : $this->themePath('images/default-emblem.png');
											$sex = $pbdata[0]['sex'] == 'F' ? 'f' : 'm';
											$class = isset($Creative->Cfg['JobAliasesGender'][$sex][$pbdata[0]['job']]) ? $Creative->Cfg['JobAliasesGender'][$sex][$pbdata[0]['job']] : $pbdata[0]['job'];
											$icon = isset($Creative->Cfg['JobAliases'][$class]) ? $Creative->Cfg['JobAliases'][$class] : $class;
											$char_url = "?module=character&action=view&id=".$pbdata[0]['char_id'];
											if( $guild_id )
												$guild_url = "?module=guild&action=view&id=".$guild_id;
											else
												$guild_url = "";
									?>
										<div class="name">
											<strong>1#</strong> <?php echo $name ?>
										</div>
										<div class="image">
											<img src="<?php echo $this->themePath('images/class/job_' . $icon . '_' . $sex . '.png') ?>" class="jobimg" />
											<div class="desc">
												<a href="<?php echo $char_url ?>"" data-toggle="tooltip" data-placement="top" data-html="true" title="<?php echo htmlspecialchars($this->jobClassText($class)) ?>"><img src="<?php echo $this->themePath('images/class/guild/' . $icon . '.png') ?>"></a>
											<?php if( $guild_id ): ?>
												<a href="<?php echo $guild_url ?>"" data-toggle="tooltip" data-placement="top" data-html="true" title="<?php echo htmlspecialchars($guild_name) ?>"><img src="<?php echo $emblem ?>"></a>
											<?php endif ?>
											</div>
										</div>
										<div class="more">
											<a href="<?php echo $char_url ?>" class="btn btn-success">Visualizar</a>
										</div>
									<?php endif ?>
									</div>
									<div class="positions">
										<table class="table">
											<thead>
												<tr>
													<th class="text-center">#</th>
													<th>Nome</th>
													<th class="text-center">Pontos</th>
												</tr>
											</thead>
											<tbody>
											<?php
												for( $i=0; $i < $Creative->Cfg['rank_home_rows']; $i++ ):
													if( isset($pbdata[$i]) ) :
														$name = htmlspecialchars($pbdata[$i]['name']);
														$guild_id = $pbdata[$i]['guild_id'] ? $pbdata[$i]['guild_id'] : 0;
														$guild_name = $pbdata[$i]['guild_name'] ? htmlspecialchars($pbdata[$i]['guild_name']) : '';
														$emblem = $guild_id && $pbdata[$i]['emblem_len'] ? $this->emblem($pbdata[$i]['guild_id']) : $this->themePath('images/default-emblem.png');
														$sex = $pbdata[$i]['sex'] == 'F' ? 'f' : 'm';
														$class = isset($Creative->Cfg['JobAliasesGender'][$sex][$pbdata[$i]['job']]) ? $Creative->Cfg['JobAliasesGender'][$sex][$pbdata[$i]['job']] : $pbdata[$i]['job'];
														$icon = isset($Creative->Cfg['JobAliases'][$class]) ? $Creative->Cfg['JobAliases'][$class] : $class;
														$char_url = "?module=character&action=view&id=".$pbdata[$i]['char_id'];
														if( $guild_id )
															$guild_url = "?module=guild&action=view&id=".$guild_id;
														else
															$guild_url = "";
											?>
											
												<tr data-action="rank-change" data-type="char" data-target="#break" data-pos="<?php echo ($i+1) ?>" data-charname="<?php echo $name ?>" data-guildname="<?php echo $guild_name ?>" data-guildid="<?php echo $guild_id ?>" data-guildemblem="<?php echo $emblem ?>" data-jobname="<?php echo htmlspecialchars($this->jobClassText($class)) ?>" data-jobicon="<?php echo $this->themePath('images/class/guild/' . $icon . '.png') ?>" data-jobimage="<?php echo $this->themePath('images/class/job_' . $icon . '_' . $sex . '.png') ?>" data-charurl="<?php echo $char_url ?>" data-guildurl="<?php echo $guild_url ?>">
													<td class="text-center"><?php echo ($i+1) ?></td>
													<td><?php echo $name ?></td>
													<td class="text-center"><?php echo number_format($pbdata[$i]['break']) ?></td>
												</tr>
												<?php else: ?>
												<tr>
													<td colspan="3">&nbsp;</td>
												</tr>
												<?php endif; ?>
											<?php endfor ?>
											</tbody>
										</table>
									</div>
								</div>
								
								<div class="content rank-wrapper ranking-hidden" id="mvp">
									<div class="view">
									<?php
										if( isset($mvpdata[0]) ):
											$name = htmlspecialchars($mvpdata[0]['name']);
											$guild_id = $mvpdata[0]['guild_id'] ? $mvpdata[0]['guild_id'] : false;
											$guild_name = $mvpdata[0]['guild_id'] && $mvpdata[0]['guild_name'] ? htmlspecialchars($mvpdata[0]['guild_name']) : '';
											$emblem = $guild_id && $mvpdata[0]['emblem_len'] ? $this->emblem($mvpdata[0]['guild_id']) : $this->themePath('images/default-emblem.png');
											$sex = $mvpdata[0]['sex'] == 'F' ? 'f' : 'm';
											$class = isset($Creative->Cfg['JobAliasesGender'][$sex][$mvpdata[0]['job']]) ? $Creative->Cfg['JobAliasesGender'][$sex][$mvpdata[0]['job']] : $mvpdata[0]['job'];
											$icon = isset($Creative->Cfg['JobAliases'][$class]) ? $Creative->Cfg['JobAliases'][$class] : $class;
											$char_url = "?module=character&action=view&id=".$mvpdata[0]['char_id'];
											if( $guild_id )
												$guild_url = "?module=guild&action=view&id=".$guild_id;
											else
												$guild_url = "";
									?>
										<div class="name">
											<strong>1#</strong> <?php echo $name ?>
										</div>
										<div class="image">
											<img src="<?php echo $this->themePath('images/class/job_' . $icon . '_' . $sex . '.png') ?>" class="jobimg" />
											<div class="desc">
												<a href="<?php echo $char_url ?>"" data-toggle="tooltip" data-placement="top" data-html="true" title="<?php echo htmlspecialchars($this->jobClassText($class)) ?>"><img src="<?php echo $this->themePath('images/class/guild/' . $icon . '.png') ?>"></a>
											<?php if( $guild_id ): ?>
												<a href="<?php echo $guild_url ?>"" data-toggle="tooltip" data-placement="top" data-html="true" title="<?php echo htmlspecialchars($guild_name) ?>"><img src="<?php echo $emblem ?>"></a>
											<?php endif ?>
											</div>
										</div>
										<div class="more">
											<a href="<?php echo $char_url ?>" class="btn btn-success">Visualizar</a>
										</div>
									<?php endif ?>
									</div>
									<div class="positions">
										<table class="table">
											<thead>
												<tr>
													<th class="text-center">#</th>
													<th>Nome</th>
													<th class="text-center">Pontos</th>
												</tr>
											</thead>
											<tbody>
											<?php
												for( $i=0; $i < $Creative->Cfg['rank_home_rows']; $i++ ):
													if( isset($mvpdata[$i]) ) :
														$name = htmlspecialchars($mvpdata[$i]['name']);
														$guild_id = $mvpdata[$i]['guild_id'] ? $mvpdata[$i]['guild_id'] : 0;
														$guild_name = $mvpdata[$i]['guild_name'] ? htmlspecialchars($mvpdata[$i]['guild_name']) : '';
														$emblem = $guild_id && $mvpdata[$i]['emblem_len'] ? $this->emblem($mvpdata[$i]['guild_id']) : $this->themePath('images/default-emblem.png');
														$sex = $mvpdata[$i]['sex'] == 'F' ? 'f' : 'm';
														$class = isset($Creative->Cfg['JobAliasesGender'][$sex][$mvpdata[$i]['job']]) ? $Creative->Cfg['JobAliasesGender'][$sex][$mvpdata[$i]['job']] : $mvpdata[$i]['job'];
														$icon = isset($Creative->Cfg['JobAliases'][$class]) ? $Creative->Cfg['JobAliases'][$class] : $class;
														$char_url = "?module=character&action=view&id=".$mvpdata[$i]['char_id'];
														if( $guild_id )
															$guild_url = "?module=guild&action=view&id=".$guild_id;
														else
															$guild_url = "";
											?>
											
												<tr data-action="rank-change" data-type="char" data-target="#mvp" data-pos="<?php echo ($i+1) ?>" data-charname="<?php echo $name ?>" data-guildname="<?php echo $guild_name ?>" data-guildid="<?php echo $guild_id ?>" data-guildemblem="<?php echo $emblem ?>" data-jobname="<?php echo htmlspecialchars($this->jobClassText($class)) ?>" data-jobicon="<?php echo $this->themePath('images/class/guild/' . $icon . '.png') ?>" data-jobimage="<?php echo $this->themePath('images/class/job_' . $icon . '_' . $sex . '.png') ?>" data-charurl="<?php echo $char_url ?>" data-guildurl="<?php echo $guild_url ?>">
													<td class="text-center"><?php echo ($i+1) ?></td>
													<td><?php echo $name ?></td>
													<td class="text-center"><?php echo number_format($mvpdata[$i]['kill']) ?>
												<?php else: ?>
												<tr>
													<td colspan="3">&nbsp;</td>
												</tr>
												<?php endif; ?>
											<?php endfor ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="wrapper-right">
							<div class="woe-info invisible" data-animation="fadeIn">
								<div class="head">
									<div class="icon">
										<img src="<?php echo $this->themePath('images/icon/event.png') ?>" />
									</div>
									<div class="title">
										<h2>Guerra do Emperium</h2>
										<span>Calendário da Guerra</span>
									</div>
								</div>
								<div class="items">
									<div class="item">
										<div class="day">Seg</div>
										<div class="content">
											<div class="title">Kriemhild</div>
											<div class="time">Das 19:00 ás 21:00 horas</div>
										</div>
									</div>
									<div class="item">
										<div class="day">Ter</div>
										<div class="content">
											<div class="title">Kriemhild</div>
											<div class="time">Das 19:00 ás 21:00 horas</div>
										</div>
									</div>
									<div class="item">
										<div class="day">Qua</div>
										<div class="content">
											<div class="title">Kriemhild</div>
											<div class="time">Das 19:00 ás 21:00 horas</div>
										</div>
									</div>
									<div class="item">
										<div class="day">Qui</div>
										<div class="content">
											<div class="title">Kriemhild</div>
											<div class="time">Das 19:00 ás 21:00 horas</div>
										</div>
									</div>
									<div class="item">
										<div class="day">Sex</div>
										<div class="content">
											<div class="title">Kriemhild</div>
											<div class="time">Das 19:00 ás 21:00 horas</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="wrapper-full invisible">
							<div class="owl-showcase" data-animation="fadeIn">
								<div class="head-wrapper">
									<div class="title">
										<div class="icon">
											<img src="<?php echo $this->themePath('images/icon/shop.png') ?>" />
										</div>
										<div class="text">
											Item Shop
										</div>
									</div>
									<div class="filter-wrapper">
										<div class="owl-showcase-nav owl-nav"></div>
								<?php if( isset($Creative->Cfg['ItemShop']['url']) && !empty($Creative->Cfg['ItemShop']['url']) ): ?>
										<a href="<?php echo $Creative->Cfg['ItemShop']['url'] ?>" class="read-more"><i class="fas fa-external-link-alt"></i></a>
								<?php endif ?>
									</div>
								</div>
								<div class="owl-carousel">
							<?php foreach( $Creative->Cfg['ItemShop']['list'] as $id => $value ): ?>
									<div class="item">
										<div class="image"><img src="<?php echo $this->itemImage($id) ?>" /></div>
										<div class="name"><?php echo utf8_encode($value['name']) ?></div>
										<div class="price"><span>Preço:</span> <?php echo number_format($value['price']) ?></div>
									</div>
							<?php endforeach ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>