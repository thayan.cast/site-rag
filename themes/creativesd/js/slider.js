/*.---------------------------------------------------------------.
  .   ____                          __                            .
  .  /\  _`\                       /\ \__  __                     .
  .  \ \ \/\_\  _ __    __     __  \ \ ,_\/\_\  __  __     __     .
  .   \ \ \/_/_/\`'__\/'__`\ /'__`\ \ \ \/\/\ \/\ \/\ \  /'__`\   .
  .    \ \ \s\ \ \ \//\  __//\ \s\.\_\ \ \_\ \ \ \ \_/ |/\  __/   .
  .     \ \____/\ \_\\ \____\ \__/.\_\\ \__\\ \_\ \___/ \ \____\  .
  .      \/___/  \/_/ \/____/\/__/\/_/ \/__/ \/_/\/__/   \/____/  .
  .                                                               .
  .         2014~2019 � Creative Services and Development         .
  .                     www.creativesd.com.br                     .
  .---------------------------------------------------------------.
  . Autor: Romulo SM (sbk_)                          Vers�o: 1.0  .
  *---------------------------------------------------------------*/
// Slider
$(document).ready(function() {
	$(".anim-slider").animateSlider(
	{
		autoplay	:true,
		interval	:8000,
		animations 	: 
		{
			0	: 	//Slide No1
			{
				'.content'	: 
				{
					show   	  : "fadeIn",
					hide 	  : "fadeOut",
					delayShow : "delay0-5s"
	 			},
				'.description'	: 
				{
					show   	  : "flipInX",
					hide 	  : "flipOutX",
					delayShow : "delay1s"
	 			},
				'.head'	: 
				{
					show   	  : "bounceIn",
					hide 	  : "flipOutX",
					delayShow : "delay1-5s"
	 			},
				'.text'	: 
				{
					show   	  : "bounceIn",
					hide 	  : "flipOutX",
					delayShow : "delay2s"
	 			}
			},
			1 :
			{
				'.content'	: 
				{
					show   	  : "fadeIn",
					hide 	  : "fadeOut",
					delayShow : "delay0-5s"
	 			},
				'.description'	: 
				{
					show   	  : "flipInX",
					hide 	  : "flipOutX",
					delayShow : "delay1s"
	 			},
				'.head'	: 
				{
					show   	  : "bounceIn",
					hide 	  : "flipOutX",
					delayShow : "delay1-5s"
	 			},
				'.text'	: 
				{
					show   	  : "bounceIn",
					hide 	  : "flipOutX",
					delayShow : "delay2s"
	 			}
			},
			2 :
			{
				'.content'	: 
				{
					show   	  : "fadeIn",
					hide 	  : "fadeOut",
					delayShow : "delay0-5s"
	 			},
				'.description'	: 
				{
					show   	  : "flipInX",
					hide 	  : "flipOutX",
					delayShow : "delay1s"
	 			},
				'.head'	: 
				{
					show   	  : "bounceIn",
					hide 	  : "flipOutX",
					delayShow : "delay1-5s"
	 			},
				'.text'	: 
				{
					show   	  : "bounceIn",
					hide 	  : "flipOutX",
					delayShow : "delay2s"
	 			}
			}
		}
	});
});