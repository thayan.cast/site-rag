<?php if (!defined('FLUX_ROOT')) exit; ?>

	</div>
	
	<!-- Go Top -->
	<a href="#" class="go-top"></a>

	<div id="footer" class="invisible" data-animation="fadeIn">
		<div class="copyright">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<p>Copyright &copy; 2020 - <a href="#"><?php echo htmlspecialchars(Flux::config('SiteTitle')) ?></a>.
						Todos Direitos Reservado.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="navigation">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="contact">
							<div class="title">Contato</div>
							<div class="mail">contato@SEU_RAG.com.br</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="logo">
							<img src="<?php echo $this->themePath('images/logo-small.png') ?>">
						</div>
						<div class="links">
							<div class="head"><?php echo utf8_encode('Navega��o') ?></div>
							<ul class="items">
								<li><a href="#"><?php echo utf8_encode('�nicio') ?></a></li>
								<li><a href="#"><?php echo utf8_encode('Informa��es') ?></a></li>
								<li><a href="#"><?php echo utf8_encode('Download') ?></a></li>
								<li><a href="#"><?php echo utf8_encode('Regras') ?></a></li>
							</ul>
							<ul class="items">
								<li><a href="#"><?php echo utf8_encode('Not�cias') ?></a></li>
								<li><a href="#"><?php echo utf8_encode('Equipe') ?></a></li>
								<li><a href="#"><?php echo utf8_encode('Doa��es') ?></a></li>
								<li><a href="#"><?php echo utf8_encode('Vote por Pontos') ?></a></li>
							</ul>
							<ul class="items">
								<li><a href="#"><?php echo utf8_encode('Registro') ?></a></li>
								<li><a href="#"><?php echo utf8_encode('Login') ?></a></li>
								<li><a href="http://forum.SEU_RAG.com.br/"><?php echo utf8_encode('Suporte') ?></a></li>
								<li><a href="http://forum.SEU_RAG.com.br/"><?php echo utf8_encode('F�rum') ?></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>


		<!--[if lt IE 9]>
		<script src="<?php echo $this->themePath('js/ie9.js') ?>" type="text/javascript"></script>
		<script type="text/javascript" src="<?php echo $this->themePath('js/flux.unitpngfix.js') ?>"></script>
		<![endif]-->
		
		<script type="text/javascript">
			function updatePreferredServer(sel){
				var preferred = sel.options[sel.selectedIndex].value;
				document.preferred_server_form.preferred_server.value = preferred;
				document.preferred_server_form.submit();
			}
			
			function updatePreferredTheme(sel){
				var preferred = sel.options[sel.selectedIndex].value;
				document.preferred_theme_form.preferred_theme.value = preferred;
				document.preferred_theme_form.submit();
			}

			// Preload spinner image.
			var spinner = new Image();
			spinner.src = '<?php echo $this->themePath('img/spinner.gif') ?>';
			
			function refreshSecurityCode(imgSelector){
				$(imgSelector).attr('src', spinner.src);
				
				// Load image, spinner will be active until loading is complete.
				var clean = <?php echo Flux::config('UseCleanUrls') ? 'true' : 'false' ?>;
				var image = new Image();
				image.src = "<?php echo $this->url('captcha') ?>"+(clean ? '?nocache=' : '&nocache=')+Math.random();
				
				$(imgSelector).attr('src', image.src);
			}
			function toggleSearchForm()
			{
				//$('.search-form').toggle();
				$('.search-form').slideToggle('fast');
			}
		</script>
		
		<?php if (Flux::config('EnableReCaptcha') && Flux::config('ReCaptchaTheme')): ?>
			<script type="text/javascript">
				 var RecaptchaOptions = {
					theme : '<?php echo Flux::config('ReCaptchaTheme') ?>'
				 };
			</script>
		<?php endif ?>
		
		<script type="text/javascript">
			var BASE_URL = '<?php echo $Creative->getURL() ?>'
			
			$(document).ready(function(){
				var inputs = 'input[type=text],input[type=password],input[type=file]';
				$(inputs).focus(function(){
					$(this).css({
						'background-color': '#f9f5e7',
						'border-color': '#dcd7c7',
						'color': '#726c58'
					});
				});
				$(inputs).blur(function(){
					$(this).css({
						'backgroundColor': '#ffffff',
						'borderColor': '#dddddd',
						'color': '#444444'
					}, 500);
				});
				$('.menuitem a').hover(
					function(){
						$(this).fadeTo(200, 0.85);
						$(this).css('cursor', 'pointer');
					},
					function(){
						$(this).fadeTo(150, 1.00);
						$(this).css('cursor', 'normal');
					}
				);
				$('.money-input').keyup(function() {
					var creditValue = parseInt($(this).val() / <?php echo Flux::config('CreditExchangeRate') ?>, 10);
					if (isNaN(creditValue))
						$('.credit-input').val('?');
					else
						$('.credit-input').val(creditValue);
				}).keyup();
				$('.credit-input').keyup(function() {
					var moneyValue = parseFloat($(this).val() * <?php echo Flux::config('CreditExchangeRate') ?>);
					if (isNaN(moneyValue))
						$('.money-input').val('?');
					else
						$('.money-input').val(moneyValue.toFixed(2));
				}).keyup();
				
				// In: js/flux.datefields.js
				processDateFields();
			});
			
			function reload(){
				window.location.href = '<?php echo $this->url ?>';
			}
		</script>
	</body>
</html>
